package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
   
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);

    @Test
    public void testBoxComplete() {
        logger.info("Test to show algorithm is wrong");

        DotsAndBoxesGrid a = new DotsAndBoxesGrid(4,4,2);
        assertTrue(a.boxComplete(0,0));

    }



    @Test
    public void testLineDrawn() {
        logger.info("Test to show if line already drawn.");

        DotsAndBoxesGrid b = new DotsAndBoxesGrid(4,4,2);
        assertThrows(RuntimeException.class, () -> b.getHorizontal(0, 0));
    }
}